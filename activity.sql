-- Return the customerName of the customers who are from the Philippines
SELECT customerName FROM customers WHERE country = "Philippines";
        -- Cruz & Sons Co.

-- Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";
        -- Labrune, Janine

-- Return the product name and MSRP of the product named "The Titanic";
SELECT productName, MSRP FROM products WHERE productName = "The Titanic"; 
        -- The Titanic, 100.17

-- Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";
        -- Jeff, Firrelli
        -- Julie, Firrelli

-- Return the names of customers who have no registered state
SELECT customerName FROM customers WHERE state IS NULL;

-- Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
SELECT firstName, lastName, email FROM employees WHERE firstName = "Steve" AND lastName = "Patterson";

-- Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- Return the customer numbers of orders whose comments contain the string 'DHL'
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- Return the product lines whose text description mentions the phrase 'state of the art'
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- Return the countries of customers without duplication
SELECT DISTINCT country FROM customers;

-- Return the statuses of orders without duplication
SELECT DISTINCT status FROM orders;

-- Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT customerName, country FROM customers WHERE country IN("USA", "France", "Canada");

-- Return the first name, last name, and office's city of employees whose offices are in Tokyo
SELECT firstName, lastName, officeCode FROM employees WHERE officeCode = 5;

-- Return the customer names of customers who were served by the employee named "Leslie Thompson"
SELECT customerName FROM customers WHERE salesRepEmployeeNumber = 1166;

-- Return the product names and customer name of products ordered by "Baane Mini Imports" (customerNumber = 121)
SELECT customers.customerName, products.productName FROM customers
    JOIN orders ON customers.customerNumber = orders.customerNumber AND customers.customerNumber = 121
    JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
    JOIN products ON orderdetails.productCode = products.productCode;

-- Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country
--employees.firstName, employees.lastName, employees.officeCode
SELECT customers.customerName, employees.firstName, employees.lastName, employees.officeCode FROM customers
    JOIN offices ON customers.country = offices.country
    JOIN employees ON offices.officeCode = employees.officeCode;

-- Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;

-- Return the customer's names with a phone number containing "+81".
SELECT customerName FROM customers WHERE phone LIKE "+81%";